# Roundcube - Ansible Role

This role covers deployment, configuration and software updates of Roundcube. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.

`vagrant up`

`ansible-playbook -b Playbooks/roundcube.yml`

Then you can access Roundcube from your computer on http://192.168.33.4


## Playbook
The playbook includes nginx, php-fpm and mariadb roles and deploys entire stack needed to run Roundcube. Additional roles are also available in the Ansible roles repos in git.

## Tags
You can use several tags when you deploy:
- `carddav` to just deploy carddav.
- `patch` to just deploy patch.
- `plugins` to just deploy plugins.
- `theme` to just deploy theme.

## OAuth
Please refer to [official documentation](https://github.com/roundcube/roundcubemail/wiki/Configuration:-OAuth2) and adjust variables accordingly. All oauth related variables can be found in `defaults/main.yml`. To toggle oauth set `rcube_oauth: true`
